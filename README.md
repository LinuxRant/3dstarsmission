# 3DStarsMission



## Free and Open-Source 3D Shootem-up

How to install? [Take me to install instructions!](#Installation)

## What can I do with this game?
First of all this is a Free and Open-Source game (licensed under GPL3 )
You can do whatever you like with this software,
just make sure you make it available for your users under GPL 3.


# Installation
```

```
Game Is in Prototyping phase.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
I am very sorry, the project didn't reach the alpha stage yet. I promise pictures and early access is coming shortly, stay in touch.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
Once I come up with an alpha version, I plan to make few versions to test them on Linux PCs, Mac, and Android devices. With mouse controls, with gamepad controls, and gyroscope if possible.

## Contributing
I am pretty much open for contribution and collaboration.
I suggest you use what I use, for good communication, but I am not a nazi, whatever floats your boat. This project is built on Linux using PyCharm or Kate with LSP Server (I use vim sometimes too) You can follow steps below, to catch up with me(debian based distro with flatpak).
I am planning to make this game available on Linux, and Android.
Probably as a executable script, an appimage and apk file first and foremost.
I would prefer you contact me during your contribution, it would be nice to talk through the vision.

### 1. Set up environment:
- [ ] pip3 install ursina jedi pysl
- [ ] flatpak install com.jetbrains.PyCharm-Community
- [ ] make a pull request
- [ ] [contact me](youtube.com/@LinuxRant) to talk about your idea
- [ ] start working on your improvements

## Authors and acknowledgment
So far, the main creator of this game is [LinuxRant](https://www.youtube.com/@linuxrant)

## License
This project is licensed under GPL 3 (Gnu Public License, version 3). You can do whatever you like with this game, just make sure what you make with it(your source-code) is easily accessible for your users and is licensed under GPL 3

## Project status
- [x] Just Started at version 0.1
- [ ] Setting up git, python environment and workflow
- [ ] coding, making assets
- [ ] coding and testing
- [ ] sharing the alpha, getting feedback
- [ ] improving the code
- [ ] final touches and beta testing
- [ ] release of version 1.0
