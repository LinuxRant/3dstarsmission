# general settings:
win-camera-fov 50
win-size 486 256
# win-size 720 405
fullscreen 0
undecorated 1
win-fixed-size 0

window-title 3D Stars Mission
show-frame-rate-meter 0
# want-tk 1
# Input map for 3DStarsMission

# Movement
#  : move_fwd
a : move_left
s : move_bwd
d : move_right
