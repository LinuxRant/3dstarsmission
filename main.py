#!/usr/bin/python3
# there is a war on consciousness going now,
# there is real war going as well.
# Make games not war
# Games are to enhance life, not replace it, go ride a real horse.
# Care for what comes into your consciousness
# Value your time, value your experiences

from math import pi, sin, cos
from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from panda3d.core import VBase4, Material, AmbientLight, PointLight
from panda3d.core import loadPrcFile
import os

loadPrcFile("conf.prc")

# os.environ['GRAPHICS_CARD'] = 'OpenGL 1.4'

keyMap = {
        "fwd": False,
        "bwd": False,
        "left": False,
        "right": False,
        "autoshoot": False,
        "nuke": False,
        "evade": False
        }


def updateKeyMap(key, state):
    keyMap[key] = state


def autoshootToggle():
    if keyMap["autoshoot"] is False:
        keyMap["autoshoot"] = True
#        print(keyMap["autoshoot"])
    else:
        keyMap["autoshoot"] = False
#        print(keyMap["autoshoot"])


class Mission(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
# Environment Setup
        self.set_background_color(0.1, 0.1, 0.1, 0)
        ambiance = AmbientLight("ambiance")
        ambiance.setColor((0.05, 0.05, 0.05, 1))
        ambiance_node = self.render.attachNewNode(ambiance)
        self.render.setLight(ambiance_node)

        light = PointLight("light")
        light.setColor((1, 1, 1, 1))
        light_node = self.render.attachNewNode(light)
        light_node.setPos(5, 50, 2)
        self.render.setLight(light_node)
        light2 = PointLight("light")
        light2.setColor((0.2, 0.2, 1, 1))
        light_node2 = self.render.attachNewNode(light2)
        light_node2.setPos(-5, 40, 2)
        self.render.setLight(light_node2)
# Camera Setup
        self.cam.setPos(0, -1, 9)
        self.cam.setP(-25)
# Ship Setup
        self.ship = self.loader.loadModel("assets/ship1b.obj")
        self.ship.reparentTo(self.render)
        self.ship.setScale(.7, .3, .3)
        self.ship.setHpr(0, 0, 0)
        self.ship.setPos(0, 10, 0)

        self.newModel = self.loader.loadModel("models/box.egg")
        mat = Material()
        self.newModel.set_material(mat)
        mat.setSpecular((1, 1, 1, 1))
        # VBase4(1, 1, 1, 1))
        mat.setShininess(5.0)
        mat.setDiffuse((1, 0, 0, 0))  # solid color
        mat.setMetallic(1)
        self.ship.set_material(mat)
        self.accept("arrow_left", updateKeyMap, ["left", True])
        self.accept("arrow_left-up", updateKeyMap, ["left", False])
        self.accept("arrow_right", updateKeyMap, ["right", True])
        self.accept("arrow_right-up", updateKeyMap, ["right", False])
        self.accept("arrow_up", updateKeyMap, ["fwd", True])
        self.accept("arrow_up-up", updateKeyMap, ["fwd", False])
        self.accept("arrow_down", updateKeyMap, ["bwd", True])
        self.accept("arrow_down-up", updateKeyMap, ["bwd", False])
        self.accept("w", autoshootToggle)
        self.accept("e", updateKeyMap, ["evade", True])
        self.accept("e-up", updateKeyMap, ["evade", False])
#        if keyMap["autoshoot"] == False:
#            self.accept("w", updateKeyMap, ["autoshoot", True])
#        if keyMap["autoshoot"] == True:
#            print(keyMap[autshoot])
        # Define a procedure to move the camera.
#        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")
#        self.camera.setPos(0, -50, 0)
#        self.camera.setHpr(0, 90, 0)
#        self.taskMgr.add(self.spinShipTask, "SpinShipTask")
        self.taskMgr.add(self.update, "update")

    def update(self, task):
        dt = globalClock.getDt()
        self.Evade(700 * dt)
        self.Lean(200 * dt)
        self.Steering(7)
        return task.cont
#    def spinCameraTask(self, task):
#        angleDegrees = task.time * 6.0
#        angleRadians = angleDegrees * (pi / 180.0)
#        self.camera.setPos(20 * sin(angleRadians), -20 * cos(angleRadians), 3)
#        self.camera.setHpr(angleDegrees, 0, 0)
#        return Task.cont
#        angleDegrees = task.time * 64.0
#        angleRadians = angleDegrees * (pi / 180.0)
#        self.ship.setHpr(0, 0, angleDegrees)

    def Steering(self, borders_range):
        dt = globalClock.getDt()
        pos = self.ship.getPos()
        self.speed = 20
        self.angle = 90
        if keyMap["left"] and pos.x > -borders_range:
            pos.x -= self.speed * dt
            # print(pos.x)
        if keyMap["right"] and pos.x < borders_range:
            pos.x += self.speed * dt
            # print(pos.x)
        if keyMap["fwd"] and pos.y < 25:
            pos.y += self.speed * dt
            # print(pos.y)
        if keyMap["bwd"] and pos.y > 10:
            pos.y -= self.speed * dt
            # print(pos.y)
        self.ship.setPos(pos)

    def Shooting(self, interval, dmg, color):
        interval = 1.0
        dmg = 1
        color = "#00ff00"
        print(f"shooting at {interval} per second, {dmg} pts of damage, color= {color}")

    def Evade(self, evade_speed):
        current_ship_roll = self.ship.getR()
        if keyMap["evade"]:
            if current_ship_roll < 90:
                self.ship.setR(current_ship_roll + evade_speed)
        
        if current_ship_roll > 0 and keyMap["evade"] is False and keyMap["right"] is False:
            self.ship.setR(current_ship_roll - evade_speed/2)
        if current_ship_roll < 0 and keyMap["left"] is False:
            self.ship.setR(current_ship_roll + evade_speed/2)

    def Lean(self, lean_speed):
        if keyMap["left"] and self.ship.getR() > -40:
             self.ship.setR(self.ship.getR() - lean_speed)
        if keyMap["right"] and self.ship.getR() < 40:
            self.ship.setR(self.ship.getR() + lean_speed)
        if self.ship.getR() < 3 and self.ship.getR() > -3:
            self.ship.setR(0)
app = Mission()
app.run()
